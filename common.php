<?php
function getBrCard()
{
    return "<br>";
}
function getBoldCard($value)
{
    return "<b>$value</b>";
}
function getUnderLineCard($value)
{
    return "<u>$value</u>";
}
function getStrikeCard($value)
{
    return "<strike>$value</strike>";
}
function getItalicCard($value)
{
    return "<i>$value</i>";
}
function printArray($array)
{
    foreach ($array as $item) {
        echo "_ $item _";
    }
}
