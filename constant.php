<?php

/*
  Để định nghĩa biến cần viết kí tự $ vào trước, ngược lại hằng thì không cần.
Hằng không thể được định nghĩa bằng phép gán đơn giản, chúng chỉ có thể được 
định nghĩa bằng cách sử dụng hàm define().

Hằng có thể được định nghĩa và truy cập bất cứ đâu mà không quan tâm đến quy 
tắc phạm vi biến.

Một hằng khi đã được định nghĩa, nó không thể định nghĩa lại hoặc hủy định nghĩa.

// Ví dụ các tên hằng hợp lệ
define("ONE",     "Ví dụ tên hằng hợp lệ 1");
define("TWO2",    "Ví dụ tên hằng hợp lệ 2");
define("THREE_3", "Ví dụ tên hằng hợp lệ 3")

// Ví dụ các tên hằng không hợp lệ
define("2TWO",    "Ví dụ tên hằng không hợp lệ 1");
define("__THREE__", "Ví dụ tên hằng không hợp lệ 2"); 
 */

// Tên hằng không bắt đầu bằng số, kí tự đặc biệt

define("PI", 3.14);
define("BEAUTIFUL_NAME", "LinhTB");
define("ENOUGHT_AGE", 18);
define("FIRST_NAME", "Tran");
define("LAST_NAME", 'LINH');

/*Hằng số Magic */
/*PHP cung cấp một số lượng lớn các hằng số được định nghĩa trước 
để bất kỳ script nào cũng có thể sử dụng nó.

Có 5 hằng số magic, thay đổi tùy thuộc vào nơi chúng được sử dụng. 
Ví dụ, giá trị của __LINE__ phụ thuộc vào dòng mà nó được sử dụng 
trong script của bạn. Các hằng đặc biệt này là phân biệt kiểu chữ. */

/*
__LINE__	Dòng hiện tại của file

__FILE__	Đường dẫn đầy đủ và tên đầy đủ của file. Nếu sử dụng 
bên trong một include thì tên của file được include sẽ được trả 
về. Từ PHP 4.0.2, __FILE__ luôn luôn chưa một đường dẫn tuyệt đối, 
trong khi ở phiên bản cũ hơn chúng chứa đường dẫn tương đối trong 
một số trường hợp

__FUNCTION__	Tên của hàm. (Được thêm trong PHP 4.3.0) Như 
PHP 5, hằng số này trả về tên của hàm như nó đã khai báo trước 
đó (phân biệt kiểu chữ). Trong PHP 4, giá trị của nó luôn là 
chữ in thường

__CLASS__	Tên của lớp. (Được thêm trong PHP 4.3.0) Như PHP 5,
 hằng số này trả về tên của lớp như nó đã khai báo trước đó 
 (phân biệt kiểu chữ). Trong PHP 4 giá trị của nó luôn là chữ in 
 thường

__METHOD__	Tên phương thức lớp. (Được thêm trong PHP 5.0.0) Tên 
phương thức này được trả về như đã khai báo trước đó (phân biệt 
kiểu chữ). 
*/
