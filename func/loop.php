<?php
class Loop
{
    public $intArray;
    public $stringArray;
    public $sortArray = array(10, 29, 1, 9, 55, 190, 56);

    public function __construct($arrayLength)
    {
        $this->initIntArray($arrayLength);
        $this->initStringArray($arrayLength);
    }

    private function initIntArray($value)
    {
        $this->intArray = array();
        for ($i = 0; $i < $value; $i++) {
            array_push($this->intArray, $i);
        }
    }
    private function initStringArray($value)
    {
        $this->stringArray = array();
        $i = 0;
        $strValue = "";
        while ($i < $value) {
            $strValue .= $i;
            array_push($this->stringArray, $strValue);
            $i++;
        }
    }
    public function sortArray()
    {
        $count = count($this->sortArray);
        $i = 0;
        echo getBrCard();
        echo getBoldCard(getUnderLineCard("Before sort"));
        echo getBrCard();
        printArray($this->sortArray);
        // do {
        for ($i = 0; $i < $count - 1; $i++) {
            for ($j = 0; $j < $count - 1; $j++) {
                if ($this->sortArray[$j] > $this->sortArray[$j + 1]) {
                    $temp = $this->sortArray[$j];
                    $this->sortArray[$j] = $this->sortArray[$j + 1];
                    $this->sortArray[$j + 1] = $temp;
                    break;
                } else {
                    continue;
                }
            }
        }
        // } while ($i < $count - 1);

        echo getBrCard();
        echo getBoldCard(getUnderLineCard("After sort"));
        echo getBrCard();
        printArray($this->sortArray);
    }
}
