<?php
$name = null;
$age = null;
$nameEx = null;
$ageEx = null;
$nameStatic = null;
$ageStatic = null;
if (isset($_GET["name"]) && isset($_GET["age"])) {
    $name = $_GET["name"];
    $age = $_GET["age"];
}
if (isset($_POST["name"]) || isset($_POST["age"])) {
    $nameEx = $_POST["name"];
    $ageEx = $_POST["age"];
}
if (isset($_REQUEST["name"]) || isset($_REQUEST["age"])) {
    $nameStatic = $_REQUEST["name"];
    $ageStatic = $_REQUEST["age"];
}
