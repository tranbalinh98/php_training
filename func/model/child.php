<?php
include '../func/model/father.php';
include '../func/interface/trans_interface.php';

class Child extends Father implements Trans
{
    public $nickName;
    public $count;
    public function __construct($nickName, $count, $bankId, $salary, $phoneNumber, $name, $age)
    {
        parent::__construct($bankId, $salary, $phoneNumber, $name, $age);
        $this->nickName = $nickName;
        $this->count = $count;
    }
    public function toString()
    {
        echo "<b>Name : $this->name :<br></b>" .
            " - nickname: $this->nickname <br>" .
            " - phone number: $this->phoneNumber <br>" .
            " - age: $this->age <br>" .
            " - count/family: $this->count <br>";
    }
    public function transMethod()
    {
        echo "CAR";
    }
}
