<?php
class Operator extends Employee
{
    private $_notEnought = "Age not enought!";
    private $_enought = "Age enought!";
    public function __construct($first_name, $last_name, $age)
    {
        parent::__construct($first_name, $last_name, $age);
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->age = $age;
    }
    public function getAgeStatus()
    {
        if ($this->age < constant("ENOUGHT_AGE")) {
            return $this->_notEnought;
        }
        return $this->_enought;
    }
    public function getAgeDiff()
    {
        if ($this->age >= constant("ENOUGHT_AGE")) {
            return $this->age - constant("ENOUGHT_AGE");
        }
        return constant("ENOUGHT_AGE") - $this->age;
    }
    public function getNameAdminDiff()
    {
        switch ($this->first_name) {
            case constant("FIRST_NAME"):
                return true;
                break;
            case constant("LAST_NAME"):
                return true;
                break;
            case constant("BEAUTIFUL_NAME");
                return true;
                break;
            default:
                return false;
        }
    }
}
