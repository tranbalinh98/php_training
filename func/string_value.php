<?php
function getStrLen($strValue)
{
    return getStrLen($strValue);
}
function insertSpecialChar($strValue)
{
    // insert [/] before text in a..z
    return addcslashes($strValue, 'a..z');
}
function changeToASCII_HEX($strValue)
{
    // change to ascii hex code
    return bin2hex($strValue);
}
function clearLastWhere($str1, $str2)
{
    //  return chop($string, $charList)
    // -Hàm này có tác dụng xóa ký tự,hoặc từ cuối cùng của 
    // chuỗi nếu nó = $charlist.
    return chop($str1, $str2);
}
function convertToInt($strValue)
{
    // convert type from string to int
    return crc32($strValue);
}
function splitStringWithSpace($strValue)
{
    //     explode($separator, $string, $limit)
    // -Hàm này có tác dụng tách chuỗi $string thành nhiều 
    // chuỗi khác với điều kiện $separator, và giới hạn $limit.
    $array = explode(' ', $strValue);
    foreach ($array as $item) {
        echo "____$item";
    }
}
function connectString($str1, $str2)
{
    // implode($separator, $array)
    // -Hàm này có tác dụng nối tất cả các phần tử của
    //  mảng $array thành chuỗi với khoảng phân biệt $separator.
    $array = array($str1, $str2);
    return implode($array);
}

function countWord($str1)
{
    //     str_word_count($string)
    // -Hàm này có tác dụng đếm xem chuỗi $string có bao nhiêu từ.
    return str_word_count($str1);
}
function repeatString($strValue, $repeatCount)
{
    //     str_repeat($string, $repeat)
    // -Hàm này có tác dụng lặp chuỗi $string $repeate lần.
    return str_repeat($strValue, $repeatCount);
}
function replaceStringWith_a_char($str1, $str2)
{
    //     str_replace($find, $replace, $string)
    // -Hàm này có tác dụng tìm kiếm chuỗi $find và thay thế
    //  chuỗi đó bằng $replace trong chuỗi $string.
    return str_replace('a', $str2, $str1);
}
function convertToMD5($strValue)
{
    return md5($strValue);
}
