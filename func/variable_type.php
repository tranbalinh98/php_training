<?php
class VariableType
{
    public $intValue = 10;
    public $doubleValue = 10.01;
    public $boolValue = true;
    public $nullValue = null;
    public $arrayValue = array(
        "key"  => 10,
        "key2" => "key",
        "key3" => 10.12
    );
    public $strValue = "Str test";

    public function __construct()
    {
    }


    /**
     * Get the value of nullValue
     */
    public function printNullValue()
    {
        $type = gettype($this->nullValue);
        return "<br>type: $type<br> ========>|value:  $this->nullValue";
    }

    /**
     * print the value of boolValue
     */
    public function printBoolValue()
    {
        $type = gettype($this->boolValue);
        return "<br>type: $type<br> ========>|value:  $this->boolValue";
    }

    /**
     * print the value of doubleValue
     */
    public function printDoubleValue()
    {
        $type = gettype($this->doubleValue);
        return "<br>type: $type<br> ========>|value:   $this->doubleValue";
    }

    /**
     * print the value of inteValue
     */
    public function printIntValue()
    {
        $type = gettype($this->intValue);
        return "<br>type: $type<br> ========>|value:   $this->intValue";
    }
    public function intWithDouble()
    {
        $sum = $this->intValue + $this->doubleValue;
        return $sum;
    }
}
