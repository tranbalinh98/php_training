<?php
include '../training/router.php';
?>
<html>

<head>
    <title>Ví dụ chương trình PHP</title>
</head>

<body>
    <div>
        <h1>LinhTB base for PHP</h1>
        <a href=" <?php echo getRouter(constant("OOP")) ?>">
            <?php echo getRouter(constant("OOP")) ?></a><br>
        <a href=" <?php echo getRouter(constant("VARIABLE")) ?>">
            <?php echo getRouter(constant("VARIABLE")) ?></a><br>
        <a href=" <?php echo getRouter(constant("LOOP_ARRAY")) ?>">
            <?php echo getRouter(constant("LOOP_ARRAY")) ?></a><br>
        <a href=" <?php echo getRouter(constant("STRING_VALUE")) ?>">
            <?php echo getRouter(constant("STRING_VALUE")) ?></a><br>
        <a href=" <?php echo getRouter(constant("METHOD")) ?>">
            <?php echo getRouter(constant("METHOD")) ?></a><br>
        <a href=" <?php echo getRouter(constant("USER_AGENT")) ?>">
            <?php echo getRouter(constant("USER_AGENT")) ?></a><br>
        <a href=" https://vietjack.com/php/bien_duoc_dinh_nghia_truoc_trong_php.jsp">
            <b>SuperGlobal</b></a><br>
        <a href=" <?php echo getRouter(constant("DATE_TIME")) ?>">
            <?php echo getRouter(constant("DATE_TIME")) ?></a><br>
        <a href=" <?php echo getRouter(constant("EXCEPTION")) ?>">
            <?php echo getRouter(constant("EXCEPTION")) ?></a><br>
    </div>
</body>

</html>