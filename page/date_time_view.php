<?php
include '../func/date_time.php';
include '../common.php';
?>
<html>

<head>
    <title>Ví dụ chương trình PHP- DateTime</title>
</head>

<body>
    <div>
        <h1>LinhTB base for PHP- DateTime</h1>
    </div>
    <div>

        <?php
        echo getBoldCard("get time from 1/1/1970 : ") . getTimeFrom1970();
        echo getBrCard();
        echo getBoldCard("Get current date value : ");
        echo getBrCard();
        getCurrentDateData();

        ?>

    </div>
</body>

</html>