<?php
include '../func/exception.php';
include '../common.php';
?>
<html>

<head>
    <title>Ví dụ chương trình PHP- Exception</title>
    <style>
        @import url("../style.css");

        p {
            color: blue;
            font-size: 16px;
        }
    </style>
</head>

<body>
    <div>
        <h1>LinhTB base for PHP- Exception</h1>
    </div>
    <div>
        <h3><i>Open file - Exception (if/else) =====================</i></h3>
        <?php
        getExceptionIfElese();
        checkErrorHandleTryCatch();
        ?>

    </div>
    <div>
        <h3><i>Custom Exception with erro func =====================</i></h3>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th align="left" width="20%">Tham số</th>
                    <th align="left" width="80%">Miêu tả</th>
                </tr>
                <tr>
                    <td>error_level</td>
                    <td>Bắt buộc - Xác định cấp độ lỗi cho lỗi tự định nghĩa (user-defined). Phải là một giá trị số</td>
                </tr>
                <tr>
                    <td>error_message</td>
                    <td>Bắt buộc - Xác định error message lỗi tự định nghĩa</td>
                </tr>
                <tr>
                    <td>error_file</td>
                    <td>Tùy ý - Xác định tên file trong đó lỗi xảy ra</td>
                </tr>
                <tr>
                    <td>error_line</td>
                    <td>Tùy ý - Xác định số dòng trong đó lỗi xảy ra</td>
                </tr>
                <tr>
                    <td>error_context</td>
                    <td>Tùy ý - Xác định một mảng chứa mọi biến và giá trị của chúng, sử dụng khi lỗi xảy ra</td>
                </tr>
            </tbody>
        </table>
        <p><b><i>
                    error_function(error_level,error_message, error_file,error_line,error_context);</i></b></p>
        <h4>Cấp độ lỗi có thể có trong PHP<h4>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th width="8%">Giá trị</th>
                            <th width="30%">Hằng số</th>
                            <th width="62%">Miêu tả</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>E_ERROR</td>
                            <td>Fatal run-time error. Các lỗi nghiêm trọng và việc thực thi script bị dừng lại</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>E_WARNING</td>
                            <td>Non-fatal run-time error. Các lỗi không nghiêm trọng và việc thực thi script không bị dừng lại</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>E_PARSE</td>
                            <td>Compile-time parse error. Lỗi về parse trong thời gian biên dịch. Các lỗi về parse này nên chỉ được tạo bởi parser</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>E_NOTICE</td>
                            <td>Run-time notice. Script tìm thấy cái gì đó mà có thể là một lỗi, nhưng cũng có thể xảy ra khi đang chạy một script một cách bình thường</td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td>E_CORE_ERROR</td>
                            <td>Các Fatal error mà xuất hiện trong khi cài đặt ban đầu của PHP</td>
                        </tr>
                        <tr>
                            <td>32</td>
                            <td>E_CORE_WARNING</td>
                            <td>Các non-fatal runtime error, xảy ra trong khi cài đặt ban đầu của PHP </td>
                        </tr>
                        <tr>
                            <td>256</td>
                            <td>E_USER_ERROR</td>
                            <td>Fatal error được tạo bởi người dùng. Nó giống như một E_ERROR được thiết lập bởi lập trình viên bởi sử dụng hàm trigger_error() trong PHP</td>
                        </tr>
                        <tr>
                            <td>512</td>
                            <td>E_USER_WARNING</td>
                            <td>Non-Fatal error được tạo bởi người dùng. Nó giống như một E_WARNING được thiết lập bởi lập trình viên bởi sử dụng hàm trigger_error() trong PHP</td>
                        </tr>
                        <tr>
                            <td>1024</td>
                            <td>E_USER_NOTICE</td>
                            <td>User-generated notice. Nó giống như một E_NOTICE được thiết lập bởi lập trình viên bởi sử dụng hàm trigger_error() trong PHP</td>
                        </tr>
                        <tr>
                            <td>2048</td>
                            <td>E_STRICT</td>
                            <td>Run-time notice. Kích hoạt để có các thay đổi gợi ý từ PHP tới code của bạn mà sẽ đảm bảo cho tính tương hợp nhất của code</td>
                        </tr>
                        <tr>
                            <td>4096</td>
                            <td>E_RECOVERABLE_ERROR</td>
                            <td>Fatal error có thể bắt. Giống một E_ERROR nhưng có thể được bắt bởi người dùng (tham khảo set_error_handler())</td>
                        </tr>
                        <tr>
                            <td>8191</td>
                            <td>E_ALL</td>
                            <td>Tất cả error và warning, ngoại trừ E_STRICT (E_STRICT sẽ là bộ phận của E_ALL như của PHP 6.0)</td>
                        </tr>
                    </tbody>
                </table>
                <?php

                ?>

    </div>
</body>

</html>