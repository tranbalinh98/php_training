<?php
include '../common.php';
include '../constant.php';
include '../func/loop.php';

$loopObject = new Loop(10);

?>
<html>

<head>
    <title>Ví dụ chương trình PHP</title>
</head>

<body>
    <div>
        <h1>LinhTB base for PHP - Loop with array</h1>
    </div>
    <div>
        <h3><b><i>Loop/ Array ===================</i></b></h3>
        <?php
        echo getBoldCard("Array int from 0 => 10");
        echo getBrCard();
        printArray($loopObject->intArray);
        echo getBrCard();

        echo getBrCard();
        var_dump($loopObject->intArray);
        echo getBrCard();
        echo getBoldCard("Array string from 0 => 10");
        echo getBrCard();
        printArray($loopObject->stringArray);
        echo getBrCard();
        echo getBrCard();
        var_dump($loopObject->stringArray);
        $loopObject->sortArray();
        ?>
    </div>
</body>

</html>