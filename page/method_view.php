<?php
include '../func/method.php';
$name = "Linhtb ";
?>
<html>

<head>
    <title>Ví dụ chương trình PHP</title>
</head>

<body>
    <div>
        <h1>LinhTB base for PHP - Method</h1><br>
        <h2>Form get</h2>
        <form action="<?php $_PHP_SELF ?>" method="GET">
            Họ tên: <input type="text" name="name" value="<?= $name ?>" />
            Tuổi: <input type="text" name="age" value="<?= $age ?>" />
            <input type="submit" />
        </form>

        <?php echo "Chào mừng  " . $name . "<br />";
        echo "Bạn " . $age . " tuổi."; ?>
    </div>
    <div>
        <h2>Form post</h2>
        <form action="<?php $_PHP_SELF ?>" method="POST">
            Họ tên: <input type="text" name="name" value="<?= $nameEx ?>" />
            Tuổi: <input type="text" name="age" value="<?= $ageEx ?>" />
            <input type="submit" />
        </form>

        <?php
        if (preg_match("/[^A-Za-z'-]/", isset($_POST['name']))) {
            echo "Chào mừng " . isset($_POST['name']) . "<br />";
            echo "Bạn " . isset($_POST['age']) . " tuổi.";
        } else {
            echo "Tên không hợp lệ" . isset($_POST['name']);
        }
        ?>
    </div>
    <div>
        <h2>Form post with method</h2>
        <form action="<?php $_PHP_SELF ?>" method="POST">
            Họ tên: <input type="text" name="name" value="<?= $nameStatic ?>" />
            Tuổi: <input type="text" name="age" value="<?= $ageStatic ?>" />
            <input type="submit" />
        </form>

        <?php
        if (preg_match("/[^A-Za-z'-]/", isset($_POST['name']))) {
            echo "Chào mừng " . isset($_POST['name']) . "<br />";
            echo "Bạn " . isset($_POST['age']) . " tuổi.";
        } else {
            echo "Tên không hợp lệ" . isset($_POST['name']);
        }
        ?>
    </div>
</body>

</html>