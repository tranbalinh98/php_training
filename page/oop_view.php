<?php
include '../func/employee.php';
include '../func/operator.php';
include '../common.php';
include '../constant.php';
include '../func/model/child.php';
$objEmployeeOne = new Employee('Aluu', 'ahihi', 34);
$objEmployeeTwo = new Employee('John', 'Smith', 340);
$operatorOne = new Operator('Linh', 'ahihi', 34);
$operatorTwo = new Operator('Tran', 'ahuuuhuuu', 14);
$child = new Child("nick name of Obj", 1, "123123", 12000000, "0985096825", "Tran ba linh", "22");

?>
<html>

<head>
    <title>OOP php</title>
</head>

<body>
    <div>
        <h1>LinhTB base for PHP - OOP View</h1>
    </div>
    <div>
        <?php
        echo ('<br>');
        echo $objEmployeeOne->getFirstName();
        echo $objEmployeeOne->getLastName();
        echo $objEmployeeOne->getAge();
        echo $objEmployeeOne->toString();
        echo ('<br>');
        echo $objEmployeeTwo->getFirstName();
        echo $objEmployeeTwo->getLastName();
        echo $objEmployeeTwo->getAge();
        echo $objEmployeeTwo->toString();
        ?>
    </div>
    <div>
        <h3><b><i>Operator ===================</i></b></h3>
        <?php
        echo $operatorOne->toString();
        echo getBoldCard("Age status : ");
        echo $operatorOne->getAgeStatus();
        echo getBrCard();

        echo getBoldCard("age diff : ");
        echo $operatorOne->getAgeDiff();
        echo getBrCard();
        echo getBoldCard("Like name admin : ");
        $operatorOne->getNameAdminDiff() ? printf("Yes!") : printf("No!");

        echo getBrCard();
        echo $operatorTwo->toString();
        echo getBoldCard("Age status : ");
        echo $operatorTwo->getAgeStatus();
        echo getBrCard();

        echo getBoldCard("age diff : ");
        echo $operatorTwo->getAgeDiff();
        echo getBrCard();
        echo getBoldCard("Like name admin : ");
        $operatorTwo->getNameAdminDiff() ? printf("Yes!") : printf("No!");
        ?>
    </div>
    <div>
        <h3><b><i>OOP view </i></b></h3>
        <?php
        $child->toString();
        $child->transMethod();
        ?>
    </div>
</body>

</html>