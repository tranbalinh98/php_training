<?php
include '../func/string_value.php';
include '../common.php';

$str1 = null;
$str2 = 'a';
if (isset($_REQUEST["str1"]) || isset($_REQUEST["str2"])) {
    $str1 = $_REQUEST["str1"];
    $str2 = $_REQUEST["str2"];
}
?>
<html>

<head>
    <title>Ví dụ chương trình PHP- String</title>
</head>

<body>
    <div>
        <h1>LinhTB base for PHP- String</h1>
    </div>
    <div>
        <h2>Form get</h2>
        <form action="<?php $_PHP_SELF ?>" method="POST">
            Str1: <input type="text" name="str1" value"<?= $str1 ?> />
            Str2: <input type="text" name="str2" value"<?= $str2 ?> />
            <input type="submit" />
        </form>

        <?php
        echo "Str1 : $str1";
        echo getBrCard();
        echo "Str2 : $str2";
        if ($str1 != null) {
            echo getBrCard();
            echo getBoldCard("Get string length \"$str1\" -(strlent) : ") . insertSpecialChar($str1);
            echo getBrCard();
            echo getBoldCard("In sert\" \\ \" before text in test1 (a..z -(addcslashes) : ") . insertSpecialChar($str1);
            echo getBrCard();
            echo getBoldCard("Con vert to ASCII HEX -(bin2hex): ") . changeToASCII_HEX($str1);
            echo getBrCard();
            echo getBoldCard("Delete last char if == \"$str2\" in \"$str1\" -(chop) : ") . clearLastWhere($str1, $str2);
            echo getBrCard();
            echo getBoldCard("Convert \" $str1\" to int -(crc32): ") . convertToInt($str1);
            echo getBrCard();
            echo getBoldCard("Split \"$str1\" by Space char -(explode) : ");
            splitStringWithSpace($str1);
            echo getBrCard();
            echo getBoldCard("Connect String \"$str1\"  vs \"$str2\" -(implode): ") . connectString($str1, $str2);
            echo getBrCard();
            echo getBoldCard("Find \"a\" in \"$str1\" and replace it by \"$str2\" -(str_replace): ") . replaceStringWith_a_char($str1, $str2);
            echo getBrCard();
            echo getBoldCard("Repeat \"$str1\" 4 times -(str_repeat): ") . repeatString($str1, 4);
            echo getBrCard();
            echo getBoldCard("Convert \"$str1\" to MD5 -(md5) : ") . convertToMD5($str1);
        }
        ?>
    </div>
</body>

</html>