<?php
include '../func/variable_type.php';
include '../common.php';
include '../constant.php';

$variableType = new VariableType();

?>
<html>

<head>
    <title>Ví dụ chương trình PHP- Variable type</title>
</head>

<body>
    <div>
        <h1>LinhTB base for PHP- VariableType</h1>
    </div>
    <div>
        <h3><b><i>Value type ===================</i></b></h3>
        <?php
        echo $variableType->printNullValue();
        echo $variableType->printBoolValue();
        echo $variableType->printIntValue();
        echo $variableType->printDoubleValue();
        echo getBrCard();
        echo ("$variableType->intValue + $variableType->doubleValue = " . $variableType->intWithDouble());
        echo getBrCard();
        var_dump($variableType->arrayValue);
        ?>
        </p>
    </div>
    <div>
        <h3><b><i>Constant ===================</i></b></h3>
        <?php
        echo getBoldCard("Pi constant :");
        echo constant("PI");
        echo getBrCard();
        echo getBoldCard("String constant :");
        echo constant("BEAUTIFUL_NAME");
        ?>
    </div>
</body>

</html>