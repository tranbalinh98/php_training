<?php
define("BASE_URL", "http://localhost:8000/training");
define("INDEX", "/index.php");
define("OOP", "/page/oop_view.php");
define("VARIABLE", "/page/variable_view.php");
define("LOOP_ARRAY", "/page/loop_array_view.php");
define("STRING_VALUE", "/page/string_view.php");
define("METHOD", "/page/method_view.php");
define("USER_AGENT", "/page/user_agent_view.php");
define("DATE_TIME", "/page/date_time_view.php");
define("EXCEPTION", "/page/exception_view.php");



function getRouter($routeName)
{
    return constant("BASE_URL") . $routeName;
}
